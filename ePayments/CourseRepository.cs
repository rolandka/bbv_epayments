namespace ePayments
{
    using System.Collections.Generic;
    using System.Linq;

    using ePayments.Models;

    public class CourseRepository : ICourseRepository
    {
        private IEnumerable<CourseModel> courses;
 
        public CourseRepository()
        {
            this.CreateCourses();
        }

        public IEnumerable<CourseModel> GetCourses()
        {
            return this.courses;
        }

        public CourseModel GetCourse(int id)
        {
            return this.courses.First(course => course.Id == id);
        }

        private void CreateCourses()
        {
            this.courses = new List<CourseModel>
                {
                    new CourseModel
                        {
                            Id = 1,
                            Title = "The Agile Development Game",
                            Description =
                                "Erleben Sie an einem konkreten Beispiel live die Auswirkungen von Scrum auf die Software-Entwicklung. Sie �ben und erleben in f�nf Tagen hochkonzentriert in f�nf Sprints, was Scrum bedeutet. Zu Beginn erarbeiten wir mit Ihnen die konkrete Planung der Software und f�llen einen initialen Backlog. An den weiteren Tagen werden dieser Backlog abgearbeitet und in Retrospektiven die eigene Arbeitsweise reflektiert. Die simulierten Kundenfeedbacks werden Ihnen wertvollen Input geben, wie mit �nderungen umzugehen ist und welche Antworten Scrum darauf hat.",
                            Price = 3100
                        },
                    new CourseModel
                        {
                            Id = 2,
                            Title = "Test Driven Development",
                            Description =
                                "Test Driven Development (TDD) ist die Basis f�r ein erfolgreiches agiles Projekt. Durch den Einsatz von TDD wird es m�glich, auf �nderungen der Anforderungen zu reagieren, ohne bestehende Funktionalit�t einzuschr�nken. Nach diesem Workshop sind Sie in der Lage, Test Driven Development in Ihrem Projekt einzusetzen. Sie verstehen sowohl den Test-First-Ablauf als auch die unterst�tzenden Tools und Frameworks.",
                            Price = 1480
                        }
                };
        }
    }
}