﻿namespace ePayments.ModelMock
{
    public class CreditCardData
    {
        public int CreditCardNumber { get; set; }

        public decimal Amount { get; set; }

        public string ProductId { get; set; }
    }
}