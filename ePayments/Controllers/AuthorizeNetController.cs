﻿using System.Web.Mvc;

namespace ePayments.Controllers
{
    using System.Globalization;

    using AuthorizeNet;

    using ePayments.ModelMock;

    public class AuthorizeNetController : Controller
    {
        /// <summary>
        /// Pays the specified id.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        public ActionResult Pay(int id)
        {
            return View(new CreditCardData { ProductId = id.ToString(CultureInfo.InvariantCulture ) });
        }

        /// <summary>
        /// Performs the transaction.
        /// </summary>
        /// <param name="creditCardNumber">The credit card number.</param>
        /// <param name="amount">The amount.</param>
        /// <param name="productId">The product id.</param>
        /// <returns></returns>
        public ActionResult PerformTransaction(int creditCardNumber, decimal amount, string productId)
        {
             // step 1 - create the request
            var request = new AuthorizationRequest(
                creditCardNumber.ToString(CultureInfo.InvariantCulture),
                "1216",
                amount,
                productId.ToString(CultureInfo.InvariantCulture));

            // step 2 - create the gateway, sending in your credentials
            var gate = new Gateway(AuthorizeNetCredentials.API_LOGIN, AuthorizeNetCredentials.TRANSACTION_KEY);

            // step 3 - make some money
            //IGatewayResponse response = gate.Send(request);

            //if (response.Approved)
            //{
            //    return View()
            //}

            return null;
        }
    }
}