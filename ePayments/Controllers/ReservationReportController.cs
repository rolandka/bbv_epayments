﻿using System.Web.Mvc;

namespace ePayments.Controllers
{
    using System.Linq;

    using ePayments.Models;

    public class ReservationReportController : Controller
    {
        private readonly ITransactionService transactionService;

        public ReservationReportController()
            : this(new TransactionService())
        {
        }

        internal ReservationReportController(ITransactionService transactionService)
        {
            this.transactionService = transactionService;
        }

        public ActionResult Reservations(int courseId)
        {
            var transactions = this.transactionService.GetTransactions(courseId).Select(transaction => new ReservationViewModel(transaction));
            return this.View(transactions);
        }
    }
}