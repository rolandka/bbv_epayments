﻿namespace ePayments
{
    using System.Collections.Generic;

    using ePayments.Models;

    public interface ICourseRepository
    {
        IEnumerable<CourseModel> GetCourses();

        CourseModel GetCourse(int id);
    }
}