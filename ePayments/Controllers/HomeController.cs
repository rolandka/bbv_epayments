﻿using System.Web.Mvc;

namespace ePayments.Controllers
{
    using Models;

    public class HomeController : Controller
    {
        private readonly ICourseRepository courseRepository;

        public HomeController() 
            : this(new CourseRepository())
        {
        }

        internal HomeController(ICourseRepository courseRepository)
        {
            this.courseRepository = courseRepository;
        }

        public ActionResult Index()
        {
            var viewModel = new HomeViewModel { Courses = courseRepository.GetCourses() };
            return View(viewModel);
        }

        public ActionResult Documentation()
        {
            return View();
        }

        public ActionResult Info()
        {
            ViewBag.Message = "Test accounts info.";

            return View();
        }
    }
}
