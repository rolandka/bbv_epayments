﻿namespace ePayments.Models
{
    using System.Collections.Generic;

    public class HomeViewModel
    {
        public IEnumerable<CourseModel> Courses { get; set; }
    }
}