﻿namespace ePayments
{
    using System.Collections.Generic;

    using AuthorizeNet;

    public interface ITransactionService
    {
        int GetNumberOfTransactions(int courseId);

        IEnumerable<Transaction> GetTransactions(int courseId);
    }
}