﻿namespace ePayments.Models
{
    public class TransactionSummaryViewModel
    {
        private readonly CourseModel course;

        public TransactionSummaryViewModel(CourseModel course, int numberOfReservations)
        {
            this.NumberOfTransactions = numberOfReservations;
            this.course = course;
        }

        public string CourseTitle
        {
            get
            {
                return this.course.Title;
            }
        }

        public int NumberOfTransactions { get; private set; }
    }
}