﻿namespace ePayments.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;

    using ePayments.Models;

    public class TransactionSummaryController : Controller
    {
        private readonly ICourseRepository courseRepository;
        private readonly ITransactionService transactionService;

        public TransactionSummaryController() 
            : this(new CourseRepository(), new TransactionService())
        {
        }

        internal TransactionSummaryController(ICourseRepository courseRepository, ITransactionService transactionService)
        {
            this.courseRepository = courseRepository;
            this.transactionService = transactionService;
        }

        public ActionResult Index()
        {
            var reservationReports = this.GetReservationReports().ToList();
            return this.View(reservationReports);
        }

        private IEnumerable<TransactionSummaryViewModel> GetReservationReports()
        {
            var courses = this.courseRepository.GetCourses();
            return
                from course in courses
                let numberOfReservations = this.transactionService.GetNumberOfTransactions(course.Id)
                select new TransactionSummaryViewModel(course, numberOfReservations);
        }

    }
}
