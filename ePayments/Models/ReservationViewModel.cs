﻿namespace ePayments.Models
{
    using AuthorizeNet;

    public class ReservationViewModel
    {
        private readonly Transaction transaction;

        public ReservationViewModel(Transaction transaction)
        {
            this.transaction = transaction;
        }

        public string CardType
        {
            get
            {
                return this.transaction.CardType;
            }
        }

        public string CardNumber
        {
            get
            {
                return this.transaction.CardNumber;
            }
        }

        public string FirstName
        {
            get
            {
                return this.transaction.FirstName;
            }
        }

        public string Lastname
        {
            get
            {
                return this.transaction.LastName;
            }
        }
    }
}