namespace ePayments
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using AuthorizeNet;

    public class TransactionService : ITransactionService
    {
        private readonly IReportingGateway reportingGateway;

        public TransactionService() 
            : this(new ReportingGateway(AuthorizeNetCredentials.API_LOGIN, AuthorizeNetCredentials.TRANSACTION_KEY))
        {
        }

        internal TransactionService(IReportingGateway reportingGateway)
        {
            this.reportingGateway = reportingGateway;
        }

        public int GetNumberOfTransactions(int courseId)
        {
            return this.reportingGateway.GetUnsettledTransactionList().Count(transaction => transaction.Description == courseId.ToString(CultureInfo.InvariantCulture));
        }

        public IEnumerable<Transaction> GetTransactions(int courseId)
        {
            return this.reportingGateway.GetUnsettledTransactionList().Where(transaction => transaction.Description == courseId.ToString(CultureInfo.InvariantCulture));
        }
    }
}